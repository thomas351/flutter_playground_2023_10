# flutter_playground_2023_10

A new Flutter project.

## Setup

```bash
flutter create flutter_playground_2023_10
cd flutter_playground_2023_10
git init --initial-branch=main
git remote add origin git@gitlab.com:thomas351/flutter_playground_2023_10.git
git config user.name "thomas351"
git config --global user.email "******@*****.**"
git add .
git commit -m "flutter create template"
git config pull.rebase true
git pull origin main
git push --set-upstream origin main
```

## Debug / Run

For comfortable debugging open in VS-Code, install Flutter extension, browse to `main.dart` and press F5.

If you instead want to run directly from terminal, use:

```bash
flutter run -d Linux --release # for Linux desktop
flutter run -d chrome --release # for browser
flutter run --release # for android if adb connected, otherwise asks to enter 1 for Linux, 2 for web. can't specify -d android, since it want's the specific adb device-id now!
```

## Release

### Android

```bash
flutter build apk --release
adb install build/app/outputs/flutter-apk/app-release.apk
```

### Linux

```bash
flutter build linux --release
build/linux/x64/release/bundle/flutter_playground
```

### Web

```bash
flutter build web --release
#npm install -g http-server # first time only
http-server -p 8000
```

## Flutter Rust integration

I have found 3 projects that offer Flutter Rust integration:

- [flutterust](https://github.com/shekohex/flutterust) does not support Web, last release is from 2020-08 and CI button is says it's build is failing.
- [flutter_rust_bridge](https://github.com/fzyzcjy/flutter_rust_bridge) ([pub.dev](https://pub.dev/packages/rust_in_flutter)) has the most likes/stars and is older (more mature?) than rust_in_flutter. It has very recent activity. CI for MacOS is currently failing. [Reddit Post 2022](https://www.reddit.com/r/rust/comments/swrbyv/write_ui_using_flutter_a_crossplatform_hotreload/)
- [rust_in_flutter](https://github.com/cunarist/rust-in-flutter) ([pub.dev](https://pub.dev/packages/rust_in_flutter)) is newer and seems to have a lot fewer dependencies than flutter_rust_bridge, at least on the Dart side of things (=what's listed on pub.dev).

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
